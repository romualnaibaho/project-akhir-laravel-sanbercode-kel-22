<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id_profile');
            $table->string('name');
            $table->string('email');
            $table->string('gender')->nullable();
            $table->string('address')->nullable();
            $table->text('bio')->nullable();
            $table->string('phone')->nullable();
            $table->string('profile_picture')->nullable();
            $table->timestamps();
            $table->foreign('id_profile')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
