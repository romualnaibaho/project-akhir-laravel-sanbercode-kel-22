@include('partials.head')
<!--<div class="se-pre-con"></div>-->
<div class="theme-layout">
	<div class="container-fluid pdng0">
		<div class="row merged">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="land-featurearea">
					<div class="land-meta">
						<h1>Sanbercode</h1>
						<p>
							Sanbercode is free to use for as long as you want with two active projects.
						</p>
						<div class="friend-logo">
							<span><img src="{{ asset('templete/images/wink.png') }}" alt=""></span>
						</div>
						<a href="#" title="" class="folow-me">Follow Us on</a>
					</div>	
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="login-reg-bg">
					<div class="log-reg-area sign">
						<h2 class="log-title">Login</h2>
							<p>
								Don’t use Sanbercode Yet? <a href="#" title="">Take the tour</a> or <a href="#" title="">Join now</a>
							</p>
						<form method="post" action="{{ route('post.login') }}">
                        @csrf
							<div class="form-group">	
							  <input type="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" name="email" id="email" required="required"/>
							  <label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
							  @if ($errors->has('email'))
                                <span class="help-block text-center">
                                    <strong style="color:red;font-size:12px;">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
							</div>
							<div class="form-group">	
							  <input type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" name="password" id="password" required="required"/>
							  <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
							  @if ($errors->has('password'))
                                <span class="help-block text-center">
                                    <strong style="color:red;font-size:12px;">{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
							</div>
							<div class="checkbox">
							  <label>
								<input type="checkbox" id="remember" name="remember" checked="checked" /><i class="check-box"></i>Always Remember Me.
							  </label>
							</div>
							<a href="#" title="" class="forgot-pwd">Forgot Password?</a>
							<div class="submit-btns">
								<button class="mtr-btn" type="submit"><span>Login</span></button>
								<a href="{{ route('register') }}"><button class="mtr-btn" type="button"><span>Register</span></button></a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
    <script src="{{ asset('templete/js/main.min.js') }}"></script>
	<script src="{{ asset('templete/js/script.js') }}"></script>

</body>	

</html>