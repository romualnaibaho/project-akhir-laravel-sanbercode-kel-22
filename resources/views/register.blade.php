@include('partials.head')
<!--<div class="se-pre-con"></div>-->
<div class="theme-layout">
	<div class="container-fluid pdng0">
		<div class="row merged">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="land-featurearea">
					<div class="land-meta">
						<h1>Sanbercode</h1>
						<p>
							Sanbercode is free to use for as long as you want with two active projects.
						</p>
						<div class="friend-logo">
							<span><img src="{{ asset('templete/images/wink.png') }}" alt=""></span>
						</div>
						<a href="#" title="" class="folow-me">Follow Us on</a>
					</div>	
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="login-reg-bg">
					<div class="log-reg-area sign">
						<h2 class="log-title">Register</h2>
							<p>
								Don’t use Sanbercode Yet? <a href="#" title="">Take the tour</a> or <a href="#" title="">Join now</a>
							</p>
						<form method="post" action="{{ route('make.register') }}">
                        @csrf
							<div class="form-group">	
							  <input type="text" class="form-control {{ $errors->has('fullname') ? ' has-error' : '' }}" name="fullname" id="fullname" required="required"/>
							  <label class="control-label " for="input">Full Name</label><i class="mtrl-select"></i>
							  @if ($errors->has('fullname'))
                                <span class="help-block text-center">
                                    <strong style="color:red;font-size:12px;">{{ $errors->first('fullname') }}</strong>
                                </span>
                            @endif
							</div>
							<div class="form-group">	
							  <input type="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" name="email" id="email" required="required"/>
							  <label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
							  @if ($errors->has('email'))
                                <span class="help-block text-center">
                                    <strong style="color:red;font-size:12px;">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
							</div>
							<div class="form-group">	
							  <input type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" name="password" id="password" required="required"/>
							  <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
							  @if ($errors->has('password'))
                                <span class="help-block text-center">
                                    <strong style="color:red;font-size:12px;">{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
							</div>
							<div class="checkbox">
							  <label>
								<input type="checkbox" checked="checked"/><i class="check-box"></i>Accept Terms & Conditions ?
							  </label>
							</div>
							<a href="{{ route('login') }}" style="color:#1FB6FF;font-size:14px;">Already have an account</a>
							<div class="submit-btns">
								<button class="mtr-btn" type="submit"><span>Register</span></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
    <script src="{{ asset('templete/js/main.min.js') }}"></script>
	<script src="{{ asset('templete/js/script.js') }}"></script>

</body>	

</html>