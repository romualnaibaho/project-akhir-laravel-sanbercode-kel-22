<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
	<title>Sanbercode Social Network Toolkit</title>
    <link rel="icon" href="{{ asset('templete/images/fav.png') }}') }}" type="image/png" sizes="16x16"> 
    
    <link rel="stylesheet" href="{{ asset('templete/css/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templete/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('templete/css/color.css') }}">
    <link rel="stylesheet" href="{{ asset('templete/css/responsive.css') }}">

</head>
<body>