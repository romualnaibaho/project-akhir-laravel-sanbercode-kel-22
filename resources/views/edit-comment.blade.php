@include('partials.head')
@include('partials.navbar')

	<section>
		<div class="gap gray-bg">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="row" id="page-contents">
							<div class="col-lg-3">
                            <aside class="sidebar static">
									<div class="widget">
										<h4 class="widget-title">Shortcuts</h4>
										<ul class="naves">
											<li>
												<i class="ti-clipboard"></i>
												<a href="newsfeed.html" title="">News feed</a>
											</li>
											<li>
												<i class="ti-mouse-alt"></i>
												<a href="inbox.html" title="">Inbox</a>
											</li>
											<li>
												<i class="ti-files"></i>
												<a href="fav-page.html" title="">My pages</a>
											</li>
											<li>
												<i class="ti-user"></i>
												<a href="timeline-friends.html" title="">friends</a>
											</li>
											<li>
												<i class="ti-image"></i>
												<a href="timeline-photos.html" title="">images</a>
											</li>
											<li>
												<i class="ti-video-camera"></i>
												<a href="timeline-videos.html" title="">videos</a>
											</li>
											<li>
												<i class="ti-comments-smiley"></i>
												<a href="messages.html" title="">Messages</a>
											</li>
											<li>
												<i class="ti-bell"></i>
												<a href="notifications.html" title="">Notifications</a>
											</li>
											<li>
												<i class="ti-share"></i>
												<a href="people-nearby.html" title="">People Nearby</a>
											</li>
											<li>
												<i class="fa fa-bar-chart-o"></i>
												<a href="insights.html" title="">insights</a>
											</li>
											<li>
												<i class="ti-power-off"></i>
												<a href="{{ route('logout') }}" title="">Logout</a>
											</li>
										</ul>
									</div><!-- Shortcuts -->
								</aside>
							</div><!-- sidebar -->
							<div class="col-lg-6">
								<div class="loadMore">
								
                                @foreach($post as $p)
									<div class="central-meta item">
										<div class="user-post">
											<div class="friend-info">
												<figure>
													<img src="{{ asset('templete/images/resources/admin.jpg') }}" alt="">
												</figure>
												<div class="friend-name">
                                                @foreach($profile as $f)
                                                @if($p->id_user == $f->id_profile)
													<ins><a href="{{ route('login') }}" title="">{{ $f->name }} </a>
                                                @endif
                                                @endforeach
                                                    </ins>
                                                    
													<span>published: {{ $p->created_at }} </span>
												</div>
												<div class="post-meta">
													<img src="{{ asset('templete/images/resources/$p->picture') }}" alt="">
                                                    <div class="description">
														
														<p style="color:black;font-size:20px;">
															{{ $p->tulisan }}
														</p>
													</div>
													<div class="we-video-info">
														<ul>
															<li>
																<span class="comment" data-toggle="tooltip" title="Comments">
																	<i class="fa fa-comments-o"></i>
																	<ins>52</ins>
																</span>
															</li>
															<li>
																<span class="like" data-toggle="tooltip" title="like">
																	<i class="ti-heart"></i>
																	<ins>{{ $p->like }}</ins>
																</span>
															</li>
															<li>
																<span class="dislike" data-toggle="tooltip" title="dislike">
																	<i class="ti-heart-broken"></i>
																	<ins>{{ $p->dislike }}</ins>
																</span>
															</li>
														</ul>
													</div>
				
												</div>
											</div>
											<div class="coment-area">
												<ul class="we-comet">
                                                @foreach($comment as $c)
                                                @if($c->id_post == $p->id_post)
													<li>
                                                    <form method="post" action="{{ route('update.comment') }}">
                                                    @csrf
														<div class="comet-avatar">
															<img src="{{ asset('templete/images/resources/admin.jpg') }}" alt="">
														</div>
                                                        
														<div class="we-comment">
															<div class="coment-head">
                                                                @foreach($profile as $f)
                                                                @if($f->id_profile == $c->id_user)
																<h5><a href="#" title="">{{ $f->name }}</a></h5>
                                                                @endif
                                                                @endforeach

																<span>{{ $c->created_at }}</span>
																<a class="we-reply" href="#" title="Reply"><i class="fa fa-reply"></i></a>
															</div>
															@foreach($comment_edit as $ce)
                                                            @if($c->id_comment != $ce->id_comment)
															<p>{{ $c->isi }}</p>
                                                            @endif
                                                            @if($c->id_comment == $ce->id_comment)
															<textarea id="isi" name="isi">{{ $c->isi }}</textarea>
                                                            <input type="hidden" id="id_comment" name="id_comment" value="{{ $c->id_comment }}">
                                                            <button type="submit" style="float:right;margin-top:6px;margin-right:5px;border-radius:7px;">Post</button>
                                                            @endif
                                                            @endforeach
														</div>
                                                        
                                                        </form>
													</li>
                                                    @endif
                                                    @endforeach
													<li class="post-comment">
														<div class="comet-avatar">
															<img src="{{ asset('templete/images/resources/admin.jpg') }}" alt="">
														</div>
														<div class="post-comt-box">
															<form method="post" action="{{ route('make.comment') }}">
                                                            @csrf
																<textarea id="comment" name="comment" placeholder="Post your comment"></textarea>
                                                                <input type="hidden" id="id_post" name="id_post" value="{{ $p->id_post }}">
																<button type="submit"><label style="color:darkblue;font-size:15px;">Post</label></button>
															</form>	
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
                                    @endforeach
                                   
								</div>
							</div><!-- centerl meta -->
							<div class="col-lg-3">
								<aside class="sidebar static">
									<div class="widget">
										<h4 class="widget-title">Your page</h4>	
										<div class="your-page">
											<figure>
												<a href="{{ route('profile') }}" title=""><img src="{{ asset('templete/images/resources/admin.jpg') }}" alt=""></a>
											</figure>
											<div class="page-meta">
												<a href="{{ route('profile') }}" title="" class="underline">{{ Auth::user()->name }}</a>
												<span><i class="ti-comment"></i><a href="insight.html" title="">Messages <em>9</em></a></span>
												<span><i class="ti-bell"></i><a href="insight.html" title="">Notifications <em>2</em></a></span>
											</div>
											<div class="page-likes">
												<ul class="nav nav-tabs likes-btn">
													<li class="nav-item"><a class="active" href="#link1" data-toggle="tab">likes</a></li>
													 <li class="nav-item"><a class="" href="#link2" data-toggle="tab">views</a></li>
												</ul>
												<!-- Tab panes -->
												<div class="tab-content">
												  <div class="tab-pane active fade show " id="link1" >
													<span><i class="ti-heart"></i>884</span>
													  <a href="#" title="weekly-likes">35 new likes this week</a>
													  <div class="users-thumb-list">
														<a href="#" title="Anderw" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-1.jpg') }}" alt="">  
														</a>
														<a href="#" title="frank" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-2.jpg') }}" alt="">  
														</a>
														<a href="#" title="Sara" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-3.jpg') }}" alt="">  
														</a>
														<a href="#" title="Amy" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-4.jpg') }}" alt="">  
														</a>
														<a href="#" title="Ema" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-5.jpg') }}" alt="">  
														</a>
														<a href="#" title="Sophie" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-6.jpg') }}" alt="">  
														</a>
														<a href="#" title="Maria" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-7.jpg') }}" alt="">  
														</a>  
													  </div>
												  </div>
												  <div class="tab-pane fade" id="link2" >
													  <span><i class="ti-eye"></i>440</span>
													  <a href="#" title="weekly-likes">440 new views this week</a>
													  <div class="users-thumb-list">
														<a href="#" title="Anderw" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-1.jpg') }}" alt="">  
														</a>
														<a href="#" title="frank" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-2.jpg') }}" alt="">  
														</a>
														<a href="#" title="Sara" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-3.jpg') }}" alt="">  
														</a>
														<a href="#" title="Amy" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-4.jpg') }}" alt="">  
														</a>
														<a href="#" title="Ema" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-5.jpg') }}" alt="">  
														</a>
														<a href="#" title="Sophie" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-6.jpg') }}" alt="">  
														</a>
														<a href="#" title="Maria" data-toggle="tooltip">
															<img src="{{ asset('templete/images/resources/userlist-7.jpg') }}" alt="">  
														</a>  
													  </div>
												  </div>
												</div>
											</div>
										</div>
									</div><!-- page like widget -->
									<div class="widget friend-list stick-widget">
										<h4 class="widget-title">Friends</h4>
										<div id="searchDir"></div>
										<ul id="people-list" class="friendz-list">
											<li>
												<figure>
													<img src="{{ asset('templete/images/resources/friend-avatar.jpg') }}" alt="">
													<span class="status f-online"></span>
												</figure>
												<div class="friendz-meta">
													<a href="time-line.html">bucky barnes</a>
													<i><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="a0d7c9ced4c5d2d3cfccc4c5d2e0c7cdc1c9cc8ec3cfcd">[email&#160;protected]</a></i>
												</div>
											</li>
											<li>
												<figure>
													<img src="{{ asset('templete/images/resources/friend-avatar2.jpg') }}" alt="">
													<span class="status f-away"></span>
												</figure>
												<div class="friendz-meta">
													<a href="time-line.html">Sarah Loren</a>
													<i><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="b4d6d5c6dad1c7f4d3d9d5ddd89ad7dbd9">[email&#160;protected]</a></i>
												</div>
											</li>
											<li>
												<figure>
													<img src="{{ asset('templete/images/resources/friend-avatar3.jpg') }}" alt="">
													<span class="status f-off"></span>
												</figure>
												<div class="friendz-meta">
													<a href="time-line.html">jason borne</a>
													<i><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="1d777c6e72737f5d7a707c7471337e7270">[email&#160;protected]</a></i>
												</div>
											</li>
											<li>
												<figure>
													<img src="{{ asset('templete/images/resources/friend-avatar4.jpg') }}" alt="">
													<span class="status f-off"></span>
												</figure>
												<div class="friendz-meta">
													<a href="time-line.html">Cameron diaz</a>
													<i><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="bed4dfcdd1d0dcfed9d3dfd7d290ddd1d3">[email&#160;protected]</a></i>
												</div>
											</li>
											<li>
												
												<figure>
													<img src="{{ asset('templete/images/resources/friend-avatar5.jpg') }}" alt="">
													<span class="status f-online"></span>
												</figure>
												<div class="friendz-meta">
													<a href="time-line.html">daniel warber</a>
													<i><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="553f34263a3b37153238343c397b363a38">[email&#160;protected]</a></i>
												</div>
											</li>
											<li>
												
												<figure>
													<img src="{{ asset('templete/images/resources/friend-avatar6.jpg') }}" alt="">
													<span class="status f-away"></span>
												</figure>
												<div class="friendz-meta">
													<a href="time-line.html">andrew</a>
													<i><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="5933382a36373b193e34383035773a3634">[email&#160;protected]</a></i>
												</div>
											</li>
											<li>
												
												<figure>
													<img src="{{ asset('templete/images/resources/friend-avatar7.jpg') }}" alt="">
													<span class="status f-off"></span>
												</figure>
												<div class="friendz-meta">
													<a href="time-line.html">amy watson</a>
													<i><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="5933382a36373b193e34383035773a3634">[email&#160;protected]</a></i>
												</div>
											</li>
											<li>
												
												<figure>
													<img src="{{ asset('templete/images/resources/friend-avatar5.jpg') }}" alt="">
													<span class="status f-online"></span>
												</figure>
												<div class="friendz-meta">
													<a href="time-line.html">daniel warber</a>
													<i><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="dbb1baa8b4b5b99bbcb6bab2b7f5b8b4b6">[email&#160;protected]</a></i>
												</div>
											</li>
											<li>
												
												<figure>
													<img src="{{ asset('templete/images/resources/friend-avatar2.jpg') }}" alt="">
													<span class="status f-away"></span>
												</figure>
												<div class="friendz-meta">
													<a href="time-line.html">Sarah Loren</a>
													<i><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="2644475448435566414b474f4a0845494b">[email&#160;protected]</a></i>
												</div>
											</li>
										</ul>
										<div class="chat-box">
											<div class="chat-head">
												<span class="status f-online"></span>
												<h6>Bucky Barnes</h6>
												<div class="more">
													<span><i class="ti-more-alt"></i></span>
													<span class="close-mesage"><i class="ti-close"></i></span>
												</div>
											</div>
											<div class="chat-list">
												<ul>
													<li class="me">
														<div class="chat-thumb"><img src="{{ asset('templete/images/resources/chatlist1.jpg') }}" alt=""></div>
														<div class="notification-event">
															<span class="chat-message-item">
																Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks
															</span>
															<span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">Yesterday at 8:10pm</time></span>
														</div>
													</li>
													<li class="you">
														<div class="chat-thumb"><img src="{{ asset('templete/images/resources/chatlist2.jpg') }}" alt=""></div>
														<div class="notification-event">
															<span class="chat-message-item">
																Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks
															</span>
															<span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">Yesterday at 8:10pm</time></span>
														</div>
													</li>
													<li class="me">
														<div class="chat-thumb"><img src="{{ asset('templete/images/resources/chatlist1.jpg') }}" alt=""></div>
														<div class="notification-event">
															<span class="chat-message-item">
																Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks
															</span>
															<span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">Yesterday at 8:10pm</time></span>
														</div>
													</li>
												</ul>
												<form class="text-box">
													<textarea placeholder="Post enter to post..."></textarea>
													<div class="add-smiles">
														<span title="add icon" class="em em-expressionless"></span>
													</div>
													<div class="smiles-bunch">
														<i class="em em---1"></i>
														<i class="em em-smiley"></i>
														<i class="em em-anguished"></i>
														<i class="em em-laughing"></i>
														<i class="em em-angry"></i>
														<i class="em em-astonished"></i>
														<i class="em em-blush"></i>
														<i class="em em-disappointed"></i>
														<i class="em em-worried"></i>
														<i class="em em-kissing_heart"></i>
														<i class="em em-rage"></i>
														<i class="em em-stuck_out_tongue"></i>
													</div>
													<button type="submit"></button>
												</form>
											</div>
										</div>
									</div><!-- friends list sidebar -->
								</aside>
							</div><!-- sidebar -->
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</section>

	@include('partials.footer')
    @include('partials.scripts')

</body>	

</html>