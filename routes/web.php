<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SanbercodeController@index')->middleware('guest');

Route::get('/login', 'SanbercodeController@login')->name('login')->middleware('guest');

Route::post('/login', 'SanbercodeController@postLogin')->name('post.login')->middleware('guest');

Route::get('/register', 'SanbercodeController@register')->name('register')->middleware('guest');

Route::post('/register', 'SanbercodeController@makeRegister')->name('make.register')->middleware('guest');

Route::get('/home', 'SanbercodeController@home')->name('home')->middleware('auth');

Route::get('/profile', 'SanbercodeController@profile')->name('profile')->middleware('auth');

Route::get('/about', 'SanbercodeController@about')->name('about')->middleware('auth');

Route::get('/edit-profile/{id}', 'SanbercodeController@editProfile')->name('edit.profile')->middleware('auth');

Route::post('/edit-profile/edit', 'SanbercodeController@makeUpdate')->name('make.update')->middleware('auth');

Route::post('/post/profile', 'SanbercodeController@makePost')->name('make.post.profile')->middleware('auth');

Route::get('/editt/post/{id}', 'SanbercodeController@editPost')->name('edit.post')->middleware('auth');

Route::get('/edit/post/{id}', 'SanbercodeController@editPostHome')->name('edit.post.home')->middleware('auth');

Route::post('/updatee/post/', 'SanbercodeController@updatePost')->name('update.post')->middleware('auth');

Route::post('/update/post/', 'SanbercodeController@updatePostHome')->name('update.post.home')->middleware('auth');

Route::get('/deletee/post/{id_post}', 'SanbercodeController@deletePost')->name('delete.post')->middleware('auth');

Route::get('/delete/post/{id_post}', 'SanbercodeController@deletePostHome')->name('delete.post.home')->middleware('auth');

Route::get('/edit/comment/{id_comment}/{id_post}', 'SanbercodeController@editComment')->name('edit.comment')->middleware('auth');

Route::get('/editt/comment/{id_comment}/{id_post}', 'SanbercodeController@editCommentHome')->name('edit.comment.home')->middleware('auth');

Route::get('/delete/comment/{id_comment}', 'SanbercodeController@deleteComment')->name('delete.comment')->middleware('auth');

Route::get('/deletee/comment/{id_comment}', 'SanbercodeController@deleteCommentHome')->name('delete.comment.home')->middleware('auth');

Route::post('/update/comment', 'SanbercodeController@updateComment')->name('update.comment')->middleware('auth');

Route::post('/updatee/comment', 'SanbercodeController@updateCommentHome')->name('update.comment.home')->middleware('auth');

Route::post('/post/home', 'SanbercodeController@makePostHome')->name('make.post.home')->middleware('auth');

Route::get('/like/{id_post}', 'SanbercodeController@likePostHome')->name('like.post.home')->middleware('auth');

Route::get('/likee/{id_post}', 'SanbercodeController@likePostProfile')->name('like.post.profile')->middleware('auth');

Route::get('/dislike/{id_post}', 'SanbercodeController@dislikePostHome')->name('dislike.post.home')->middleware('auth');

Route::get('/dislikee/{id_post}', 'SanbercodeController@dislikePostProfile')->name('dislike.post.profile')->middleware('auth');

Route::post('/comment', 'SanbercodeController@makeComment')->name('make.comment')->middleware('auth');

Route::get('/like/comment/{id_comment}', 'SanbercodeController@likeCommentProfile')->name('like.comment.profile')->middleware('auth');

Route::get('/likes/comment/{id_comment}', 'SanbercodeController@likeCommentHome')->name('like.comment.home')->middleware('auth');

Route::get('/follow/{id}', 'SanbercodeController@followHome')->name('follow.home')->middleware('auth');

Route::get('/unfollow/{id}', 'SanbercodeController@unfollowHome')->name('unfollow.home')->middleware('auth');

Route::get('/follows/{id}', 'SanbercodeController@followProfile')->name('follow.profile')->middleware('auth');

Route::get('/unfollows/{id}', 'SanbercodeController@unfollowProfile')->name('unfollow.profile')->middleware('auth');

Route::get('/logout', 'SanbercodeController@logout')->name('logout')->middleware('auth');
