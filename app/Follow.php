<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $fillable = [
        'id', 'id_user', 'follow'
    ];

    protected $hidden = [
        
    ];
}
