<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Post;
use App\Comment;
use App\Follow;

class SanbercodeController extends Controller
{
    public function index(){
        return view('login');
    }

    public function login(){
        return view('login');
    }

    public function postLogin(Request $request){

        $this->validate($request, [
            'email' => 'email|exists:users,email',
            'password' => 'required',
        ]);
        
        $attemps = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if(\Auth::attempt($attemps, true)){
            return redirect()->route('home');
        }
        
        return redirect()->back();
        
    }

    public function register(){
        return view('register');
    }

    public function makeRegister(Request $request){

        $this->validate($request, [
            'fullname' => 'required|min:4',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|'
        ]);

        User::create([
            'name' => $request->fullname,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        Profile::create([
			'name' => $request->fullname,
            'email' => $request->email
		]);

        return redirect()->route('login');
    }

    public function home(){
        $user = User::where('id', '!=', \Auth::user()->id)->orderBy('id', 'desc')->get();
        
        $post = Post::orderBy('id_post', 'desc')->get();

        $profile = Profile::orderBy('id_profile', 'desc')->get();

        $comment = Comment::orderBy('id_comment', 'asc')->get();

        $follow = Follow::where('id_user', \Auth::user()->id)
            ->get();

        $total_comment = Comment::groupBy('id_post')
                ->selectRaw('count(*) as total, id_post')
                ->get();

        return view('home', compact('post', 'comment', 'profile', 'user', 'total_comment', 'follow'));

    }

    public function makePost(Request $request){
        
        Post::create([
            'tulisan' => $request->tulisan,
            'picture' => $request->picture,
            'picture_capt' => $request->tulisan,
            'id_user' => \Auth::user()->id
        ]);

        return redirect()->route('profile');
    }

    public function makePostHome(Request $request){
        
        Post::create([
            'tulisan' => $request->tulisan,
            'picture' => $request->picture,
            'picture_capt' => $request->tulisan,
            'id_user' => \Auth::user()->id
        ]);

        return redirect()->route('home');
    }

    public function likePostHome($id_post){

        $like = Post::where('id_post', $id_post)
                ->get('like');

        $total_like = ($like[0]['like']) + 1;

        Post::where('id_post', $id_post)
                ->update([
                    'like' => $total_like
                ]);
                
        return redirect()->route('home');
    }

    public function likePostProfile($id_post){

        $like = Post::where('id_post', $id_post)
                ->get('like');

        $total_like = ($like[0]['like']) + 1;

        Post::where('id_post', $id_post)
                ->update([
                    'like' => $total_like
                ]);
                
        return redirect()->route('profile');
    }

    public function dislikePostHome($id_post){

        $like = Post::where('id_post', $id_post)
                ->get('like');

        $dislike = Post::where('id_post', $id_post)
                ->get('dislike');

        if($like[0]['like'] == 0){
            $total_like = 0;
        }else{
            $total_like = ($like[0]['like']) - 1;
        }
        
        if($like[0]['like'] == 0){
            $total_dislike = ($dislike[0]['dislike']);
        }else{
            $total_dislike = ($dislike[0]['dislike']) + 1;
        }

        Post::where('id_post', $id_post)
                ->update([
                    'like' => $total_like,
                    'dislike' => $total_dislike
                ]);
                
        return redirect()->route('home');
    }

    public function dislikePostProfile($id_post){

        $like = Post::where('id_post', $id_post)
                ->get('like');

        $dislike = Post::where('id_post', $id_post)
                ->get('dislike');

        if($like[0]['like'] == 0){
            $total_like = 0;
        }else{
            $total_like = ($like[0]['like']) - 1;
        }

        $total_dislike = ($dislike[0]['dislike']) + 1;

        Post::where('id_post', $id_post)
                ->update([
                    'like' => $total_like,
                    'dislike' => $total_dislike
                ]);
                
        return redirect()->route('profile');
    }

    public function editPost($id){

        $user = User::where('id', '!=', \Auth::user()->id)->orderBy('id', 'desc')->get();

        $profile = Profile::where('id_profile', \Auth::user()->id )->get();

        $post = Post::where('id_post', $id)->get();

        $comment = Comment::orderBy('id_comment', 'asc')->get();

        return view('edit-post', compact('user', 'profile', 'post', 'comment'));
    }

    public function editPostHome($id){

        $user = User::where('id', '!=', \Auth::user()->id)->orderBy('id', 'desc')->get();

        $profile = Profile::where('id_profile', \Auth::user()->id )->get();

        $post = Post::where('id_post', $id)->get();

        $comment = Comment::orderBy('id_comment', 'asc')->get();

        return view('edit-post-home', compact('user', 'profile', 'post', 'comment'));
    }

    public function updatePost(Request $request){

        Post::where('id_post', $request->id_post)
            ->update([
                'tulisan' => $request->tulisan,
                'picture_capt' => $request->tulisan
            ]);

        return redirect()->route('profile');
    }

    public function updatePostHome(Request $request){

        Post::where('id_post', $request->id_post)
            ->update([
                'tulisan' => $request->tulisan,
                'picture_capt' => $request->tulisan
            ]);

        return redirect()->route('home');
    }

    public function deletePost($id_post){

        Comment::where('id_post', $id_post)
            ->delete();

        Post::where('id_post', $id_post)
            ->delete();

        return redirect()->route('profile');
    }

    public function deletePostHome($id_post){

        Comment::where('id_post', $id_post)
            ->delete();

        Post::where('id_post', $id_post)
            ->delete();

        return redirect()->route('home');
    }

    public function makeComment(Request $request){
        
        Comment::create([
            'isi' => $request->comment,
            'id_user' => \Auth::user()->id,
            'id_post' => $request->id_post
        ]);

        return redirect()->back();
    }

    public function editComment($id_comment, $id_post){

        $profile = Profile::orderBy('id_profile', 'desc' )->get();

        $post = Post::where('id_post', $id_post)->get();

        $comment = Comment::orderBy('id_comment', 'asc')->get();

        $comment_edit = Comment::where('id_comment', $id_comment)->get();

        return view('edit-comment', compact('profile', 'post', 'comment', 'comment_edit'));
    }

    public function editCommentHome($id_comment, $id_post){

        $profile = Profile::orderBy('id_profile', 'desc' )->get();

        $post = Post::where('id_post', $id_post)->get();

        $comment = Comment::orderBy('id_comment', 'asc')->get();

        $comment_edit = Comment::where('id_comment', $id_comment)->get();

        return view('edit-comment-home', compact('profile', 'post', 'comment', 'comment_edit'));
    }

    public function deleteComment($id_comment){

        Comment::where('id_comment', $id_comment)->delete();

        return redirect()->route('profile');
    }

    public function deleteCommentHome($id_comment){

        Comment::where('id_comment', $id_comment)->delete();

        return redirect()->route('home');
    }

    public function updateComment(Request $request){

        Comment::where('id_comment', $request->id_comment)
            ->update([
                'isi' => $request->isi
            ]);

        return redirect()->route('profile');
    }

    public function updateCommentHome(Request $request){

        Comment::where('id_comment', $request->id_comment)
            ->update([
                'isi' => $request->isi
            ]);

        return redirect()->route('home');
    }

    public function likeCommentProfile($id_comment){

        $like = Comment::where('id_comment', $id_comment)
                ->get('like');

        $total_like = $like[0]['like'] + 1;

        Comment::where('id_comment', $id_comment)
            ->update([
                'like' => $total_like
            ]);

        return redirect()->route('profile');
    }

    public function likeCommentHome($id_comment){

        $like = Comment::where('id_comment', $id_comment)
                ->get('like');

        $total_like = $like[0]['like'] + 1;

        Comment::where('id_comment', $id_comment)
            ->update([
                'like' => $total_like
            ]);

        return redirect()->route('home');
    }

    public function profile(){

        $user = User::where('id', '!=', \Auth::user()->id)->orderBy('id', 'desc')->get();

        $post = Post::where('id_user', \Auth::user()->id )->orderBy('id_post', 'desc')->get();

        $profile = Profile::where('id_profile', \Auth::user()->id )->get();

        $all_profile = Profile::orderBy('id_profile', 'asc' )->get();

        $comment = Comment::orderBy('id_comment', 'asc')->get();

        $follow = Follow::all();

        $following = Follow::groupBy('id_user')
            ->selectRaw('count(follow) as following, id_user')
            ->get();

        $followers = Follow::groupBy('follow')
            ->selectRaw('count(id_user) as followers, follow')
            ->get();

        $total_comment = Comment::groupBy('id_post')
                ->selectRaw('count(*) as total, id_post')
                ->get();

        return view('profile', compact('post', 'comment', 'user','profile', 'all_profile', 'total_comment', 'follow', 'followers', 'following'));
    }

    public function editProfile($id){

        $profile = Profile::where('id_profile', $id )->get();

        return view('edit-profile', compact('profile'));
    }

    public function makeUpdate(Request $request){

        User::where('id', \Auth::user()->id)
            ->update([
                'name' =>  $request->fullname
        ]);

        Profile::where('id_profile', \Auth::user()->id)
            ->update([
                'name' => $request->fullname,
                'gender' => $request->gender,
                'phone' => $request->phone,
                'address' => $request->address,
                'bio' => $request->bio
		]);

        return redirect()->route('about');
    }

    public function followHome($id){

        Follow::create([
            'id_user' => \Auth::user()->id,
            'follow' => $id
        ]);

        return redirect()->route('home');
    }

    public function unfollowHome($id){

        Follow::where('follow', $id)
            ->delete();

        return redirect()->route('home');
    }

    public function followProfile($id){

        Follow::create([
            'id_user' => \Auth::user()->id,
            'follow' => $id
        ]);

        return redirect()->route('profile');
    }

    public function unfollowProfile($id){

        Follow::where('follow', $id)
            ->delete();

        return redirect()->route('profile');
    }

    public function about(){

        $profile = Profile::where('id_profile', \Auth::user()->id )->get();

        return view('about', compact('profile'));
    }

    public function logout(){
        \Auth::logout();

        return redirect()->route('login');
    }
}