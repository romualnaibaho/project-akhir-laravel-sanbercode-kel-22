<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'id_post', 'tulisan', 'picture', 'picture_capt', 'quotes', 'like', 'dislike', 'id_user'
    ];

    protected $hidden = [
        
    ];

    public function user(){

        return $this->belongsTo('App\User');

    }

    public function comment(){

        return $this->hasMany('App\Comment');

    }
}
