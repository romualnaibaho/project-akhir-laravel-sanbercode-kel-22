<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'id_profile', 'name', 'email', 'gender', 'address', 'bio', 'phone', 'profile_picture', 'followers', 'following', 'id_user'
    ];

    protected $hidden = [
        
    ];

    public function user(){

        return $this->belongsTo('App\User');

    }

}
