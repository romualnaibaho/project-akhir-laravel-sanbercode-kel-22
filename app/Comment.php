<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'id_comment', 'isi', 'like', 'id_user', 'id_post'
    ];

    protected $hidden = [
        
    ];

    public function user(){

        return $this->belongsTo('App\User');

    }

    public function post(){

        return $this->belongsTo('App\Post');

    }
}
